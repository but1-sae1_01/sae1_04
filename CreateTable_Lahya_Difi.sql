CREATE TABLE BUS(
   Immat CHAR(7),
   MiseEnService DATE NOT NULL,
   PRIMARY KEY(Immat)
);

CREATE TABLE CONDUCTEUR(
   NoPermis VARCHAR(40),
   DatePermis DATE NOT NULL,
   LieuDelivrancePermis VARCHAR(50),
   DatePriseFonction DATE NOT NULL,
   Nom VARCHAR(50) NOT NULL,
   Prenom VARCHAR(50) NOT NULL,
   DateNaissance DATE NOT NULL,
   Adresse VARCHAR(50),
   CodePostal CHAR(5),
   Ville VARCHAR(50),
   PRIMARY KEY(NoPermis)
);

CREATE TABLE LIGNE(
   NoLigne SMALLINT,
   NomArretDepart CHAR(50) not null,
   NomArretTerminus CHAR(50) not null,
   PRIMARY KEY(NoLigne)
);

CREATE TABLE TIERS(
   NoTiers INTEGER,
   NomTiers VARCHAR(50) NOT NULL,
   PrenomTiers VARCHAR(50) NOT NULL,
   DateNaissance DATE NOT NULL,
   AdresseTiers VARCHAR(50),
   CPTiers CHAR(5),
   VilleTiers VARCHAR(50),
   PRIMARY KEY(NoTiers)
);

CREATE TABLE FOURNITURE(
   CodeF CHAR(10),
   LibelleF VARCHAR(50) NOT NULL,
   PrixUnitaireCourant NUMERIC(6,2) NOT NULL,
   PRIMARY KEY(CodeF)
);

CREATE TABLE MOE(
   CodeM CHAR(10),
   LibelleM VARCHAR(50) NOT NULL,
   TauxHoraireCourant NUMERIC(5,2) NOT NULL,
   PRIMARY KEY(CodeM)
);

CREATE TABLE INTEMPERIE(
   CodeIntemp CHAR(10),
   PRIMARY KEY(CodeIntemp)
);

CREATE TABLE ACCIDENT(
   Annee INTEGER,
   NoSeq INTEGER,
   DateAccident DATE,
   HeureAccident TIME,
   Rue VARCHAR(50) NOT NULL,
   CodePostal CHAR(5),
   Ville VARCHAR(50),
   ResumeCirconstances TEXT,
   DateOuvertureDossier DATE,
   DateTransmissionAssur DATE,
   DateValidationAssur DATE,
   DateClotureDossier DATE,
   SommeRecue NUMERIC(15,2) NOT NULL,
   CodeIntemp CHAR(10),
   PRIMARY KEY(Annee, NoSeq),
   FOREIGN KEY(CodeIntemp) REFERENCES INTEMPERIE(CodeIntemp)
);

CREATE TABLE PARCOURS(
   NoPermis VARCHAR(40),
   HoraireService TIMESTAMP,
   Immat CHAR(7) NOT NULL,
   NoLigne SMALLINT NOT NULL,
   PRIMARY KEY(NoPermis, HoraireService),
   FOREIGN KEY(NoPermis) REFERENCES CONDUCTEUR(NoPermis),
   FOREIGN KEY(Immat) REFERENCES BUS(Immat),
   FOREIGN KEY(NoLigne) REFERENCES LIGNE(NoLigne)
);

CREATE TABLE INTERVENTION(
   NoRep SMALLINT,
   DateEntreeBus DATE,
   Description TEXT,
   DateDevis DATE NOT NULL,
   DateDebutReparation DATE,
   DateFinReparation DATE,
   Immat CHAR(7) NOT NULL,
   Annee INTEGER,
   NoSeq INTEGER,
   PRIMARY KEY(NoRep),
   FOREIGN KEY(Immat) REFERENCES BUS(Immat),
   FOREIGN KEY(Annee, NoSeq) REFERENCES ACCIDENT(Annee, NoSeq)
);

CREATE TABLE Survenir(
   Annee INTEGER,
   NoSeq INTEGER,
   NoPermis VARCHAR(40),
   HoraireService TIMESTAMP,
   DommagesBus TEXT,
   DommagesConducteur TEXT,
   SignatureConducteur BOOLEAN default false,
   SignatureControleur BOOLEAN default false,
   PRIMARY KEY(Annee, NoSeq, NoPermis, HoraireService),
   FOREIGN KEY(Annee, NoSeq) REFERENCES ACCIDENT(Annee, NoSeq),
   FOREIGN KEY(NoPermis, HoraireService) REFERENCES PARCOURS(NoPermis, HoraireService)
);

CREATE TABLE Impliquer(
   Annee INTEGER,
   NoSeq INTEGER,
   NoTiers INTEGER,
   NatureBlessures TEXT,
   DegatsMateriels TEXT,
   SommeVersee NUMERIC(15,2),
   PRIMARY KEY(Annee, NoSeq, NoTiers),
   FOREIGN KEY(Annee, NoSeq) REFERENCES ACCIDENT(Annee, NoSeq),
   FOREIGN KEY(NoTiers) REFERENCES TIERS(NoTiers)
);

CREATE TABLE Utiliser(
   NoRep SMALLINT,
   CodeF CHAR(10),
   QteDevis NUMERIC(5,2) NOT NULL,
   QteValidee NUMERIC(5,2),
   PrixUnitaire NUMERIC(6,2) NOT NULL,
   PRIMARY KEY(NoRep, CodeF),
   FOREIGN KEY(NoRep) REFERENCES INTERVENTION(NoRep),
   FOREIGN KEY(CodeF) REFERENCES FOURNITURE(CodeF)
);

CREATE TABLE Necessiter(
   NoRep SMALLINT,
   CodeM CHAR(10),
   NbHDevis NUMERIC(5,2) NOT NULL,
   NbHValidees NUMERIC(5,2),
   TauxHoraire NUMERIC(5,2) NOT NULL,
   PRIMARY KEY(NoRep, CodeM),
   FOREIGN KEY(NoRep) REFERENCES INTERVENTION(NoRep),
   FOREIGN KEY(CodeM) REFERENCES MOE(CodeM)
);
