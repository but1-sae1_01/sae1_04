--ETAPE1: Replir la déclation d'accident 
	--Insertion dans Accident 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier, CodeIntemp) 
values ('2023', 1, '2023-05-20', '15:59', 'Magnolia', 26000, 'Valence', 'Bus a percuté un cycliste', '2023-05-20' , 'Neige'); 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier) 
values ('2023', 2, '2023-12-01', '12:24', 'Moulins', 26000, 'Valence', 'Bus a écrasé un chien', '2023-12-01'); 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier) 
values ('2023', 3, '2023-04-10', '15:01', 'Mozart', 26000, 'Valence', 'Collision bus avec voiture lorsque le bus se remet sur la voie après s être arrêté à l arrêt Europe', '2023-04-10'); 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier, CodeIntemp) 
values ('2023', 4, '2023-12-01', '09:50', 'Rousseau', 26000, 'Valence', 'Bus glisse sur une chaussée verglacée et heurte un poteau de signalisation', '2023-12-01', 'Verglas'); 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier) 
values ('2023', 5, '2023-04-10', '13:00', 'Descartes', 26000, 'Valence', 'Bus percute l arrière d une voiture arrêtée à un feu rouge, causé par un freinage défaillant', '2023-04-10'); 
insert into accident (Annee, Noseq, DateAccident, HeureAccident, Rue, CodePostal, Ville, ResumeCirconstances, DateOuvertureDossier, CodeIntemp) 
values ('2023', 6, '2023-05-20', '16:20', 'Diderot', 26000, 'Valence', 'Bus scolaire impliqué dans un carambolage avec plusieurs véhicules lors d une forte pluie', '2023-05-20', 'Pluie'); 

	--Insertion dans Survenir 
insert into survenir (Annee, NoSeq, NoPermis, HoraireService, DommagesBus) 
values ('2023', 1, 67890, '2023-05-20 15:45:00.000', 'Carosserie légèrement rayéé du côté droit'); 
insert into survenir (Annee, NoSeq, NoPermis, HoraireService, DommagesBus, DommagesConducteur) 
values ('2023', 3, 12345, '2023-04-10 12:30:00.000', 'Carrosserie et pare-brise à remplacer' ,'Traummatisme crânien'); 
insert into survenir (Annee, NoSeq, NoPermis, HoraireService, DommagesBus, DommagesConducteur) 
values ('2023', 2, 48965, '2023-12-01 08:05:00.000', 'Pneu dégonflé' ,'Choc psychologique'); 
insert into survenir (Annee, NoSeq, NoPermis, HoraireService, DommagesBus, DommagesConducteur) 
values ('2023', 4, 66218, '2023-04-24 7:30:00.000', 'Vitre brisée' ,'Hémoragie externe sur le bras gauche'); 
insert into survenir (Annee, NoSeq, NoPermis, HoraireService, DommagesBus, DommagesConducteur) 
values ('2023', 6, 83456, '2023-12-12 6:30:00.000', 'Capo endommage et fissure sur le pare-brise' ,'Léger mal de crâne'); 

	--Insertion dans Tiers 
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('01', 'Kebaili', 'Josua', '2003-03-17', 'rue du Moulin', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('02', 'Chalencon', 'Yoan', '2005-07-25', 'rue des Marguerites', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('03', 'Difi', 'Camélia', '1997-02-01', 'rue Marcel Pagnol', '07250', 'Le Pouzin');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('04', 'Boulanouar', 'Silia', '1990-08-29', 'allée Alexandre', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('05', 'Jay', 'Quentin', '1985-03-20', 'rue Émile Gay', '26281', 'Romans-sur-Isère');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('06', 'Berrington', 'Cécile', '1933-04-21', 'rue Jean Vilar', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('07', 'Ginier-Gillet', 'Fantin', '2002-05-16', 'rue Sully', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('08', 'Mehdi', 'Kylian', '1999-07-14', 'rue du Fulton', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('09', 'Kaya', 'Hebru', '1997-06-13', 'Boulevard du Grand', '26320', 'Saint-Marcel-lès-Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('10', 'Darona', 'Lucas', '2005-10-12', 'Avenue Sadi Carnot', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('11', 'Mmadi', 'Ousslim', '2002-02-22', 'Avenue de Verdun', '26000', 'Valence');
insert into tiers (notiers, nomtiers, prenomtiers, datenaissance, adressetiers, cptiers, villetiers) 
values('12', 'Jay', 'Quentin', '1985-03-20', 'rue Émile Gay', '26281', 'Romans-sur-Isère');

	--Insertion dans Impliquer 
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','1','03','trauma cranien, perte d un oeil, hémorragie interne, hématome, 4ème molaire déchaussée','vélo professionnel irrecuperable');
insert into impliquer (annee, noseq, notiers, natureblessures) 
values('2023','2','08','chien décédé et propriétaire indèmne');
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','3','09','cheville foulée, pied cassé, hématomes, épaule luxée','voirure écrasée contre un grillage, toute la carrosserie est endommagée');
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','4','04','déplacement de la cinquième vertèbre, fracture main droite, nez cassé','ordinateur de travail cassé');
insert into impliquer (annee, noseq, notiers, natureblessures) 
values('2023','4','05','mal de tête');
insert into impliquer (annee, noseq, notiers, natureblessures) 
values('2023','4','06','fracture de la mâchoire, côtes brisées');
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','5','12','mort cérébrale, actuellement dans le coma','véhicule de travail irrecuperable');
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','6','01','trauma cranien', 'ordinateur personnel endommagé');
insert into impliquer (annee, noseq, notiers, natureblessures) 
values('2023','6','02','machoire déplacée');
insert into impliquer (annee, noseq, notiers, natureblessures,degatsmateriels) 
values('2023','6','07','Morceau de verre platé dans le bras droit','téléphone et ordinateurs personnels cassés');
insert into impliquer (annee, noseq, notiers, natureblessures) 
values('2023','6','10','poignet gauche fracturé');
insert into impliquer (annee, noseq, notiers,degatsmateriels) 
values('2023','6','11','iPad endommagé');

--ETAPE2: 
	--Impression des déclarations 
select distinct noseq, nom, prenom, nopermis, dateaccident, heureaccident, rue, accident.codepostal, accident.ville, resumecirconstances, dateouverturedossier, codeintemp, dommagesbus, dommagesconducteur 
from conducteur join survenir using(NoPermis) join accident using(NoSeq) 
order by dateaccident; 

	--Signatures 
update survenir 
set signatureconducteur = true 
where annee = 2023; 

update survenir 
set signaturecontroleur = true 
where noseq = 6 or noseq = 1; --on en a mis que 2, car " éventuellement contresignée par un contrôleur CTVR" (cf sujet)

--ETAPE3: dressage du devis
	--Insertion dans Intervention 
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq) 
values (1, '21/05/2023', 'Peindre ce qui est rayé', 'DEF5678', '2023', 1); 
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq)  
values (2, '10/04/2023', 'Carrosserie et pare-brise à remplacer', 'ABC1234', '2023', 2); 
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq)  
values (3, '12/12/2023', 'Capo et pare-brise à remplacer', 'YBD1534', '2023', 3);
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq) 
values (4, '21/05/2023', 'Réparer carrosserie et changer les freins', 'DEF5678', '2023', 4); 
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq)  
values (5, '10/04/2023', 'Réparer carrosserie et changer les freins', 'ABC1234', '2023', 5); 
insert into Intervention (NoRep , DateEntreeBus , Description, Immat, Annee, NoSeq)  
values (6, '12/12/2023', 'Réparer carrosserie et changer le moteur', 'YBD1534', '2023', 6);
	--Insertion dans Utiliser 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (1, 'F8', 1, 90.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (2, 'F6', 1, 250.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (2, 'F7', 1, 1500.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (3, 'F6', 1, 250.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (3, 'F7', 1, 1500.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (4, 'F7', 1, 1500.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (4, 'F1', 4, 40.99); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (5, 'F7', 1, 1500.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (5, 'F1', 4, 40.99); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (6, 'F7', 1, 1500.00); 
insert into utiliser (NoRep, CodeF, QteDevis, PrixUnitaire) 
values (6, 'F5', 1, 3728.75); 
	--Insertion dans Necessiter 
		-- Pour le mécanicien 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (2, 'M2', 3.00, 20.5); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (3, 'M2', 4.00, 20.5); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (4, 'M2', 2.50, 3.15); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (5, 'M2', 2.50, 3); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (6, 'M2', 18, 25); 
	-- Pour le peintre de carrosserie 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (1, 'M4', 2.00, 35); 
	-- Pour le carrossier
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (2, 'M1', 6.00, 15); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (3, 'M1', 9.00, 15);
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (4, 'M1', 4.00, 20); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (5, 'M1', 6.00, 18); 
insert into necessiter (NoRep, CodeM, NbHDevis, TauxHoraire) 
values (6, 'M1', 8.00, 19); 


	--ajout de la date du devis
update Intervention
set dateDevis = '2023-05-22'
where norep = 1;

update Intervention
set dateDevis = '2023-04-10'
where norep = 2;

update Intervention
set dateDevis = '2023-12-12'
where norep = 3;

update Intervention
set dateDevis = '2023-05-22'
where norep = 4;

update Intervention
set dateDevis = '2023-04-10'
where norep = 5;

update Intervention
set dateDevis = '2023-12-12'
where norep = 6;


	--Liste des travaux et leurs coûts par bus
select intervention.NoRep, intervention.Immat, intervention.DateEntreeBus, intervention.Description,
(select sum(utiliser.prixunitaire * utiliser.qtedevis) from utiliser where utiliser.NoRep = intervention.NoRep) as CoutTotalFournitures,
(select sum(MOE.TauxHoraireCourant * necessiter.nbHDevis) from necessiter join MOE using (CodeM) where necessiter.NoRep = intervention.NoRep) as CoutTotalMainOeuvre,
(select sum(utiliser.prixunitaire * utiliser.qtedevis) from utiliser where utiliser.NoRep = intervention.NoRep) +
(select sum(MOE.TauxHoraireCourant * necessiter.nbHDevis) from necessiter join MOE using (CodeM) where necessiter.NoRep = intervention.NoRep) as CoutTotalReparation
from intervention
order by intervention.NoRep;


--ETAPE4: Fiche de réparation
	--liste des fournitures
select norep, immat, dommagesbus, dateentreebus, libelleF, qteDevis, prixunitaireCourant
from accident join survenir using(annee, noseq) join intervention using(annee, noseq) join utiliser using(norep) join fourniture using(codeF);

	--liste de la main d'oeuvre
select norep, immat, dommagesbus,dateentreebus, libelleM, nbhDevis, tauxhoraire
from accident join survenir using(annee, noseq) join intervention using(annee, noseq) join necessiter using(norep) join moe using(codeM);

	--ajout de la date de transmission du dossier à l'assurance
update Accident
set datetransmissionassur = '2023-05-22'
where noseq = 1;

update Accident
set datetransmissionassur = '2023-04-10'
where noseq = 2;

update Accident
set datetransmissionassur = '2023-12-12'
where noseq = 3;

update Accident
set datetransmissionassur = '2023-05-22'
where noseq = 4;

update Accident
set datetransmissionassur = '2023-04-10'
where noseq = 5;

update Accident
set datetransmissionassur = '2023-12-12'
where noseq = 6;


--ETAPE5: 
	--ajout de la date de validation de l'assurance
update Accident
set datevalidationassur = '2023-05-23'
where noseq = 1;

update Accident
set datevalidationassur = '2023-04-16'
where noseq = 2;

update Accident
set datevalidationassur = '2023-12-13'
where noseq = 3;

update Accident
set datevalidationassur = '2023-05-24'
where noseq = 4;

update Accident
set datevalidationassur = '2023-04-16'
where noseq = 5;

update Accident
set datevalidationassur = '2023-12-14'
where noseq = 6;

	--ajout du nombre d'heures validées par l'assurance
update necessiter 
set nbhvalidees = 2
where norep = 1 and codem ='M4';

update necessiter 
set nbhvalidees = 3
where norep = 2 and codem ='M2';

update necessiter 
set nbhvalidees = 5.50
where norep = 2 and codem ='M1';

update necessiter 
set nbhvalidees = 2
where norep = 3 and codem ='M2';

update necessiter 
set nbhvalidees = 9
where norep = 3 and codem ='M1';

update necessiter 
set nbhvalidees = 2
where norep = 4 and codem ='M2';

update necessiter 
set nbhvalidees = 7.15
where norep = 4 and codem ='M1';

update necessiter 
set nbhvalidees = 2.5
where norep = 5 and codem ='M2';

update necessiter 
set nbhvalidees = 6
where norep = 5 and codem ='M1';

update necessiter 
set nbhvalidees = 18
where norep = 6 and codem ='M2';

update necessiter 
set nbhvalidees = 8
where norep = 6 and codem ='M1';

	--ajout les qualités validées par l'assurance
update utiliser 
set qtevalidee = 4
where codef = 'F1';

update utiliser 
set qtevalidee = 1
where codef = 'F7' or codef = 'F5' or codef = 'F6' or codef = 'F8';

	--Début des réparation
update Intervention
set datedebutreparation = '2023-05-27'
where norep = 1;

update Intervention
set datedebutreparation = '2023-04-15'
where norep = 2;

update Intervention
set datedebutreparation = '2023-12-19'
where norep = 3;

update Intervention
set datedebutreparation = '2023-05-28'
where norep = 4;

update Intervention
set datedebutreparation = '2023-04-15'
where norep = 5;

update Intervention
set datedebutreparation = '2023-12-14'
where norep = 6;

	--Fin des réparation
update Intervention
set datefinreparation = '2023-05-27'
where norep = 1;

update Intervention
set datefinreparation = '2023-04-17'
where norep = 2;

update Intervention
set datefinreparation = '2023-12-20'
where norep = 3;

update Intervention
set datefinreparation = '2023-05-30'
where norep = 4;

update Intervention
set datefinreparation = '2023-04-18'
where norep = 5;

update Intervention
set datefinreparation = '2023-12-17'
where norep = 6;

	--Somme recue par l'assurnace : Intervention + pour les tiers
		--les assurances des personnes sont chères
update Accident
set sommerecue = 65021.01
where noseq = 1;

update Accident
set sommerecue = 6954.22
where noseq = 2;

update Accident
set sommerecue = 12472.56
where noseq = 3;

update Accident
set sommerecue = 21562.99
where noseq = 4;

update Accident
set sommerecue = 156332.75 --assuranceVie du tiers chère
where noseq = 5;

update Accident
set sommerecue = 33551.22
where noseq = 6;

	--Somme versée à chaque tiers
update impliquer 
set sommeversee = 6257.22 
where notiers = 1;

update impliquer
set sommeversee = 1500.74
where notiers = 2;

update impliquer
set sommeversee = 33592.04
where notiers = 3;

update impliquer
set sommeversee = 2375.12
where notiers = 4;

update impliquer
set sommeversee = 552.45
where notiers = 5;

update impliquer
set sommeversee = 10002.00
where notiers = 6;

update impliquer 
set sommeversee = 1854.26
where notiers = 7;

update impliquer
set sommeversee = 3622.12
where notiers = 8;

update impliquer
set sommeversee = 23598.22
where notiers = 9;

update impliquer
set sommeversee = 12593.14
where notiers = 10;

update impliquer
set sommeversee = 2548.55
where notiers = 11;

update impliquer
set sommeversee = 120593.28 --assuranceVie chère
where notiers = 12;

--ETAPE 6: clôture du dossier
update accident 
set datecloturedossier = '2023-05-31'
where noseq = 1;

update accident
set datecloturedossier = '2023-04-20'
where noseq = 2;

update accident
set datecloturedossier = '2024-01-05'
where noseq = 3;

update accident
set datecloturedossier = '2023-06-23'
where noseq = 4;

update accident
set datecloturedossier = '2023-06-25' --longue durée des soins
where noseq = 5;

update accident
set datecloturedossier = '2023-12-26'
where noseq = 6;







