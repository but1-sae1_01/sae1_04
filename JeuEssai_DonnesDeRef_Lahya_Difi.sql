insert into bus (Immat, MiseEnService) values ('ABC1234', '01-01-2017');
insert into bus (Immat, MiseEnService) values ('DEF5678', '15-02-2022');
insert into bus (Immat, MiseEnService) values ('GHI9101', '30-03-2023');
insert into bus (Immat, MiseEnService) values ('CP859FR', '03-12-2018');

insert into conducteur (NoPermis, DatePermis, LieuDelivrancePermis, DatePriseFonction, Nom, Prenom, DateNaissance, Adresse, CodePostal, Ville) values ('12345', '01-01-2022', 'Marseille', '01-01-2020', 'Dupond', 'André', '15-01-1990', '26 rue des moulins', '26000', 'Valence');
insert into conducteur (NoPermis, DatePermis, LieuDelivrancePermis, DatePriseFonction, Nom, Prenom, DateNaissance, Adresse, CodePostal, Ville) values ('67890', '15-02-2000', 'Lyon', '15-02-2018', 'Bernard', 'Louise', '26-05-1985', '456 avenue g.peri', '26000', 'Valence');
insert into conducteur (NoPermis, DatePermis, LieuDelivrancePermis, DatePriseFonction, Nom, Prenom, DateNaissance, Adresse, CodePostal, Ville) values ('48965', '08-12-1999', 'Valence', '25-05-2022', 'Sanchez', 'Carlos', '01-11-1979', '12 rue derodon', '26000', 'Valence');

insert into ligne (NoLigne, NomArretDepart, NomArretTerminus) values (1, 'Sadi Carnot', 'Europe');
insert into ligne (NoLigne, NomArretDepart, NomArretTerminus) values (2, 'Thabord', 'Mozart');
insert into ligne (NoLigne, NomArretDepart, NomArretTerminus) values (3, 'Louis Blanc', 'Odysseum');

insert into fourniture (CodeF, LibelleF, PrixUnitaireCourant) values ('F1', 'Volant', 40.99);
insert into fourniture (CodeF, LibelleF, PrixUnitaireCourant) values ('F2', 'Jante', 135.49);
insert into fourniture (CodeF, LibelleF, PrixUnitaireCourant) values ('F3', 'Siege', 28.75);
insert into fourniture (CodeF, LibelleF, PrixUnitaireCourant) values ('F4', 'Huile moteur', 66.95);
insert into fourniture (CodeF, LibelleF, PrixUnitaireCourant) values ('F5', 'Moteur', 3728.75);

insert into moe (CodeM, LibelleM, TauxHoraireCourant) values ('M1', 'Equipe1', 15.00);
insert into moe (CodeM, LibelleM, TauxHoraireCourant) values ('M2', 'Equipe2', 20.50);
insert into moe (CodeM, LibelleM, TauxHoraireCourant) values ('M3', 'Equipe3', 18.75);

update moe
set LibelleM = 'Electriciens'
where CodeM = 'M3';

update moe
set LibelleM = 'Mecaniciens'
where CodeM = 'M2';

update moe
set LibelleM = 'Carrossier'
where CodeM = 'M1';

insert into intemperie (CodeIntemp) values ('Pluie');
insert into intemperie (CodeIntemp) values ('Verglas');
insert into intemperie (CodeIntemp) values ('Neige');

insert into parcours (NoPermis, HoraireService, Immat, NoLigne) values ('12345', '10-04-2023 12:30:00', 'ABC1234', 1);
insert into parcours (NoPermis, HoraireService, Immat, NoLigne) values ('67890', '20-05-2023 15:45:00', 'DEF5678', 2);
insert into parcours (NoPermis, HoraireService, Immat, NoLigne) values ('48965', '01-12-2023 08:05:00', 'GHI9101', 3);

-- Intervention n'est pas une donnée de référence

-- Tiers n'est pas une donnée de référence

-- Accident n'est pas une donnée de référence

-- Necessiter n'est pas une donnée de référence

-- Impliquer n'est pas une donnée de référence

-- Utliser n'est pas une donnée de référence

-- Necessiter n'est pas une donnée de référence
