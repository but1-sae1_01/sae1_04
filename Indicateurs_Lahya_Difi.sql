--________________________________STATISTIQUES_______________________________________--

--Le nombre d'accidents par ligne
select count(noseq) as nbaccidents, noligne
from parcours join survenir using(NoPermis)
group by noligne;

--Le conducteur avec la plus grande ancienneté
select nom, prenom, nopermis, dateprisefonction
from conducteur
where dateprisefonction = (select min(dateprisefonction) from conducteur);

--Moyenne des sommes versées par accident
select impliquer.annee, impliquer.noseq, avg(sommeversee) as SommeMoyenne 
from impliquer join accident on accident.annee = impliquer.annee and accident.noseq = impliquer.noseq 
group by impliquer.annee, impliquer.noseq
order by impliquer.noseq asc;

--Le coût moyen des formitures par intervention
select sum(qtevalidee * prixunitairecourant) as TotalDesCoûts, norep
from utiliser join fourniture using(codeF)
group by norep
order by TotalDesCoûts;

--L'accident avec la plus grande somme versée et l'identité du tiers
select nomtiers, prenomtiers, datenaissance, sommeversee 
from impliquer join tiers using(notiers)
where sommeversee = (select max(sommeversee) from impliquer);